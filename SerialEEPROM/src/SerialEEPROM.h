#ifndef SERIALEEPROM_H
#define SERIALEEPROM_H

#include <libopencm3/stm32/gpio.h>
#include <delay.h>
#include <customSPI.h>
// #include <USBClass.h>

class SerialEEPROMChip{
    public:
        SerialEEPROMChip(uint32_t spi, uint32_t ssPort, uint16_t ssPin);

        bool begin();
	    bool begin(uint32_t spi, uint32_t ssPort, uint16_t ssPin);

        void writeByte(uint8_t addr, uint8_t dat);
        void write(uint8_t addr, uint8_t *buf, uint8_t len);

        uint8_t readByte(uint8_t addr);
        void read(uint8_t addr, uint8_t *buf, uint8_t len);
    
    private:
        uint32_t SPIPort;
        uint32_t nssPort;
        uint16_t nssPin;

        void wait();
        void writeEnable();
        void writeDisable();
};

#endif