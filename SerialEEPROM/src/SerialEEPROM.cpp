#include <SerialEEPROM.h>

#define READ            0x03
#define WRITE           0x02
#define WRITE_DISABLE   0x04
#define WRITE_ENABLE    0x06
#define READ_STATUS     0x05
#define WRITE_STATUS    0x01

SerialEEPROMChip::SerialEEPROMChip(uint32_t spi, uint32_t ssPort, uint16_t ssPin){
    SPIPort          = spi;
    nssPort          = ssPort;
    nssPin           = ssPin;
}

bool SerialEEPROMChip::begin(){
    gpio_mode_setup(nssPort, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, nssPin);
    gpio_set(nssPort, nssPin);  

    wait();
    
    uint8_t status;

    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, WRITE_ENABLE);
    gpio_set(nssPort, nssPin);

    delayMicroseconds(1);

    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, READ_STATUS);
    status = spi_readwrite(SPIPort, 0x00);
    gpio_set(nssPort, nssPin);

    delayMicroseconds(1);

    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, WRITE_DISABLE);
    gpio_set(nssPort, nssPin);

    if (status & 0x02){
        return true;
    }
    return false;
}

bool SerialEEPROMChip::begin(uint32_t spi, uint32_t ssPort, uint16_t ssPin){
    SPIPort          = spi;
    nssPort          = ssPort;
    nssPin           = ssPin;

    return begin();
}

void SerialEEPROMChip::writeByte(uint8_t addr, uint8_t dat){
    wait();

    writeEnable();

    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, WRITE);
    spi_readwrite(SPIPort, addr);
    spi_readwrite(SPIPort, dat);
    gpio_set(nssPort, nssPin);

    delayMicroseconds(1);
}

void SerialEEPROMChip::write(uint8_t addr, uint8_t *buf, uint8_t len){
    wait();

    writeEnable();

    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, WRITE);
    spi_readwrite(SPIPort, addr);
    while(len > 0){
        spi_readwrite(SPIPort, *buf++);
        len--; 
        addr++;
        // new page, new write
        if (addr % 16 == 0){   
            gpio_set(nssPort, nssPin);
            delay(5);
            wait(); 
            writeEnable();
            gpio_clear(nssPort, nssPin);
            spi_readwrite(SPIPort, WRITE);
            spi_readwrite(SPIPort, addr);
        }
    }
    gpio_set(nssPort, nssPin);

    delay(5);

    wait();  

    writeDisable();
    delayMicroseconds(1);
}

uint8_t SerialEEPROMChip::readByte(uint8_t addr){
    uint8_t dat = 0;

    wait();

    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, READ);
    spi_readwrite(SPIPort, addr);
    dat = spi_readwrite(SPIPort, 0x00);
    gpio_set(nssPort, nssPin);

    return dat;
}

void SerialEEPROMChip::read(uint8_t addr, uint8_t *buf, uint8_t len){
    wait();

    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, READ);
    spi_readwrite(SPIPort, addr);
    while (len > 0){
        *buf++ = spi_readwrite(SPIPort, 0x00);
        len--;
    }
    gpio_set(nssPort, nssPin);
    delayMicroseconds(1);

}

void SerialEEPROMChip::wait(void){
    uint8_t status = 0x01;
    while(status & 0x01){
        gpio_clear(nssPort, nssPin);
        spi_readwrite(SPIPort, READ_STATUS);
        status = spi_readwrite(SPIPort, 0x00);
	    gpio_set(nssPort, nssPin);
        delayMicroseconds(1);
    }
}

void SerialEEPROMChip::writeEnable(void){
    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, WRITE_ENABLE);
    gpio_set(nssPort, nssPin);
    delayMicroseconds(1);
}

void SerialEEPROMChip::writeDisable(void){
    gpio_clear(nssPort, nssPin);
    spi_readwrite(SPIPort, WRITE_DISABLE);
    gpio_set(nssPort, nssPin);
    delayMicroseconds(1);
}
