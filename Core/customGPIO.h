#ifndef CUSTOMGPIO_H
#define CUSTOMGPIO_H

#include <libopencm3/stm32/gpio.h>
#include <stdint.h>

#define LOW     0
#define HIGH    1

struct gpio_struct{
    uint32_t port;
    uint16_t pin;
};

void digitalWrite(uint32_t port, uint16_t pin, uint8_t state);

uint16_t digitalRead(uint32_t port, uint16_t pin);

// void gpio_mode_setup(struct gpio_struct gpio, uint8_t mode, uint8_t pull_up_down);

// void gpio_toggle(struct gpio_struct gpio);

// void gpio_set(struct gpio_struct gpio);

// void gpio_clear(struct gpio_struct gpio);

// uint16_t gpio_get(struct gpio_struct gpio);


#endif