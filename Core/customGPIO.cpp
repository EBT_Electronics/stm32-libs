#include <customGPIO.h>

void digitalWrite(uint32_t port, uint16_t pin, uint8_t state){
    if (state){
        gpio_set(port, pin);
    }
    else{
        gpio_clear(port, pin);
    }
}

uint16_t digitalRead(uint32_t port, uint16_t pin){
    return gpio_get(port, pin);
}

// void gpio_mode_setup(struct gpio_struct gpio, uint8_t mode, uint8_t pull_up_down){
//     gpio_mode_setup(gpio.port, mode, pull_up_down, gpio.pin);
// }

// void gpio_toggle(struct gpio_struct gpio){
//     gpio_toggle(gpio.port, gpio.pin);
// }

// void gpio_set(struct gpio_struct gpio){
//     gpio_set(gpio.port, gpio.pin);
// }

// void gpio_clear(struct gpio_struct gpio){
//     gpio_clear(gpio.port, gpio.pin);
// }

// uint16_t gpio_get(struct gpio_struct gpio){
// 	return gpio_get(gpio.port, gpio.pin);
// }