#ifndef CHECKSUM_H
#define CHECKSUM_H

uint8_t calculateCRC8(char *c){
    uint8_t cc = 0;
    while (*c != 0){
        cc ^= *c;
        c++;
    }
    return cc;
}

uint8_t calculateCRC8(char *c, uint16_t len){
    uint8_t cc = 0;
    while (len > 0){
        cc ^= *c;
        c++;
        len--;
    }
    return cc;
}

#endif