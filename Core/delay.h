#ifndef INCLUDE_DELAY_H
#define INCLUDE_DELAY_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"{
#endif 

// The apllication is responsible for incrementing varMil every millisecond and varMu everey microsecond
extern volatile unsigned long varMil;
extern volatile unsigned long varMu;

/* sleep for delay milliseconds */
void delay(unsigned long delay);

void delayMicroseconds(unsigned long delay);

unsigned long millis(void);
unsigned long micros(void);

#ifdef __cplusplus
}
#endif

#endif