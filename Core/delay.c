#include <delay.h>

/* sleep for delay milliseconds */
void delay(unsigned long delay){
	uint32_t wake = varMil + delay;
	while (wake > varMil);
}

/* sleep for delay microseconds */
void delayMicroseconds(unsigned long delay){
	uint32_t wake = varMu + delay;
	while (wake > varMu);
}

unsigned long millis(void){
	return varMil;
}

unsigned long micros(void){
    return varMu;
}