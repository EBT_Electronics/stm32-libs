#ifndef SSD1309_H
#define SSD1309_H

#include <stdint.h>
#include <gfxfont.h>
#include "Print.h"

#define Set_Lower_Column_Start_Address_CMD        0x00
#define Set_Higher_Column_Start_Address_CMD       0x10
#define Set_Memory_Addressing_Mode_CMD            0x20
#define Set_Column_Address_CMD                    0x21
#define Set_Page_Address_CMD                      0x22
#define Set_Display_Start_Line_CMD                0x40
#define Set_Contrast_Control_CMD                  0x81
#define Set_Charge_Pump_CMD                       0x8D
#define Set_Segment_Remap_CMD                     0xA0
#define Set_Entire_Display_ON_CMD                 0xA4
#define Set_Normal_or_Inverse_Display_CMD         0xA6
#define Set_Multiplex_Ratio_CMD                   0xA8
#define Set_Display_ON_or_OFF_CMD                 0xAE
#define Set_Page_Start_Address_CMD                0xB0
#define Set_COM_Output_Scan_Direction_CMD         0xC0
#define Set_Display_Offset_CMD                    0xD3
#define Set_Display_Clock_CMD                     0xD5
#define Set_Pre_charge_Period_CMD                 0xD9
#define Set_Common_HW_Config_CMD                  0xDA
#define Set_VCOMH_Level_CMD                       0xDB
#define Set_NOP_CMD                               0xE3

#define Normal_Display                            0x00
#define Entire_Display_ON                         0x01
                                                    
#define Non_Inverted_Display                      0x00
#define Inverted_Display                          0x01
                    
#define Column_Address_0_Mapped_to_SEG0           0x00
#define Column_Address_0_Mapped_to_SEG127         0x01

#define Display_OFF                               0x00
#define Display_ON                                0x01

#define Scan_from_COM0_to_63                      0x00
#define Scan_from_COM63_to_0                      0x08

#define x_size                                    128
#define x_max                                     x_size
#define x_min                                     0
#define y_size                                    64
#define y_max                                     8
#define y_min                                     0

#define ON                                        1
#define OFF                                       0

#define YES                                       1
#define NO                                        0

#define HIGH                                      1
#define LOW                                       0

#define ROUND                                     1
#define SQUARE                                    0

#define DAT                                       1
#define CMD                                       0

#define buffer_size                               1024//(x_max * y_max)

extern void ssd1309_spi_out(uint8_t d);

class SSD1309 : public Print {
    public:
        void init(uint32_t CSPort, uint16_t CSPin, uint32_t RSTPort, uint16_t RSTPin, uint32_t CMDPort, uint16_t CMDPin);
        void reset_sequence(void);
        void writeCmd(uint8_t value, uint8_t type);
        void gotoxy(unsigned char x_pos, unsigned char y_pos);
        void fill(unsigned char bmp_data);
        void clear_screen();
        void clear_buffer();

        void
            setCursor(uint8_t x, uint8_t y),
            drawPixel(uint8_t x, uint8_t y, uint8_t color),
            drawLine(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color),
            drawFastVLine(uint8_t x, uint8_t y, uint8_t h, uint8_t color),
            drawFastHLine(uint8_t x, uint8_t y, uint8_t h, uint8_t color),
            drawRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color),
            fillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color),
            drawCircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color),
            fillCircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color),
            fillCircleHelper(uint8_t x0, uint8_t y0, uint8_t r, uint8_t cornername, uint8_t delta, uint8_t color);

        void
            setTextHorizontalCenter(bool c),
            setTextAlignRight(uint8_t x),
            setFont(const GFXfont *f = NULL),
            setTextColor(uint8_t c),
            drawChar(uint8_t x, uint8_t y, unsigned char c, uint8_t color, uint8_t size),
            customprint(char *t),
            drawBitmap(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h),
            show(void);
            
            virtual size_t write(uint8_t);

    private:
        uint32_t cs_port;
        uint16_t cs_pin;
        uint32_t rst_port;
        uint16_t rst_pin;
        uint32_t cmd_port;
        uint16_t cmd_pin;

        uint8_t buffer[buffer_size];

        uint8_t 
            getCharWidth(char c),
            getCharWidthAndSpace(char c);

        uint8_t
            _width, _height, // Display w/h as modified by current rotation
            cursor_x, cursor_y,
            align_x;

        uint8_t
            textcolor;

        uint8_t
            textsize;

        bool
            wrap,   // If set, 'wrap' text at right edge of display
            horizontalCenter, // if set, center text horizontally
            verticalCenter, // if set, center text vertically
            _cp437; // If set, use correct CP437 charset (default is off)

        GFXfont
            *gfxFont;

        void gpio_init(void);
};


extern SSD1309 oled;

#endif