#include <SSD1309.h>

#include <stdlib.h> 

#include <libopencm3/stm32/gpio.h>
#include <customGPIO.h>
#include <delay.h>

#include "glcdfont.c"

#ifndef pgm_read_byte
 #define pgm_read_byte(addr) (*(const unsigned char *)(addr))
#endif
#ifndef pgm_read_word
 #define pgm_read_word(addr) (*(const unsigned short *)(addr))
#endif
#ifndef pgm_read_dword
 #define pgm_read_dword(addr) (*(const unsigned long *)(addr))
#endif

#if !defined(__INT_MAX__) || (__INT_MAX__ > 0xFFFF)
 #define pgm_read_pointer(addr) ((void *)pgm_read_dword(addr))
#else
 #define pgm_read_pointer(addr) ((void *)pgm_read_word(addr))
#endif

#ifndef _swap_int16_t
#define _swap_int16_t(a, b) { int16_t t = a; a = b; b = t; }
#endif

SSD1309 oled;

void SSD1309::init(uint32_t CSPort, uint16_t CSPin, uint32_t RSTPort, uint16_t RSTPin, uint32_t CMDPort, uint16_t CMDPin){
    cs_port = CSPort;
    cs_pin = CSPin;
    rst_port = RSTPort;
    rst_pin = RSTPin;
    cmd_port = CMDPort;
    cmd_pin = CMDPin;
    _width    = 128;
    _height   = 64;
    _cp437    = false;
    cursor_y  = cursor_x    = 0;
    textcolor = 1;
    textsize  = 1;
    horizontalCenter = 0;
    align_x = 0;

    uint8_t rotate = 0;

    gpio_init();

    reset_sequence();
    
    writeCmd(0xFD, CMD);
    writeCmd(0x12, CMD);

    writeCmd((Set_Display_ON_or_OFF_CMD + Display_OFF) , CMD);
    
    writeCmd(Set_Display_Clock_CMD, CMD);
    writeCmd(0x80, CMD);

    writeCmd(Set_Multiplex_Ratio_CMD, CMD);
    writeCmd(0x3F, CMD);

    writeCmd(Set_Display_Offset_CMD, CMD);
    writeCmd(0x00, CMD);
    
    writeCmd((Set_Display_Start_Line_CMD | 0x00), CMD);

    if (rotate == 0){
        writeCmd((Set_Segment_Remap_CMD | Column_Address_0_Mapped_to_SEG127), CMD);
    
        writeCmd((Set_COM_Output_Scan_Direction_CMD | Scan_from_COM63_to_0), CMD);
    }
    else{
        writeCmd((Set_Segment_Remap_CMD | Column_Address_0_Mapped_to_SEG0), CMD);
    
        writeCmd((Set_COM_Output_Scan_Direction_CMD | Scan_from_COM0_to_63), CMD);
    }

    
    writeCmd(Set_Common_HW_Config_CMD, CMD);
    writeCmd(0x12, CMD);

    writeCmd(Set_Contrast_Control_CMD, CMD);
    writeCmd(0xFF, CMD);
    
    writeCmd(Set_Pre_charge_Period_CMD, CMD);
    writeCmd(0xF1, CMD);
    
    writeCmd(Set_VCOMH_Level_CMD, CMD);
    writeCmd(0x3C, CMD);

    writeCmd((Set_Entire_Display_ON_CMD | Normal_Display), CMD);

    writeCmd((Set_Normal_or_Inverse_Display_CMD | Non_Inverted_Display), CMD);

    writeCmd((Set_Display_ON_or_OFF_CMD + Display_ON) , CMD);
    
    gotoxy(0, 0);
    
    clear_buffer();
    clear_screen();
}

void SSD1309::reset_sequence(void){
    delay(10);
    digitalWrite(rst_port, rst_pin, 0);
    delay(10);
    digitalWrite(rst_port, rst_pin, 1);
}

void SSD1309::writeCmd(uint8_t value, uint8_t type){
    digitalWrite(cmd_port, cmd_pin, type & 0x01);
    digitalWrite(cs_port, cs_pin, 0);
    delayMicroseconds(25);
    ssd1309_spi_out(value);
    delayMicroseconds(25);
    digitalWrite(cs_port, cs_pin, 1);
}

void SSD1309::gotoxy(unsigned char x_pos, unsigned char y_pos){  
    cursor_x = x_pos;
    cursor_y = y_pos;                                  
    writeCmd((Set_Page_Start_Address_CMD + y_pos), CMD);
    writeCmd(((x_pos & 0x0F) | Set_Lower_Column_Start_Address_CMD), CMD);
    writeCmd((((x_pos & 0xF0) >> 0x04) | Set_Higher_Column_Start_Address_CMD), CMD);
}

void SSD1309::fill(unsigned char bmp_data){                                                    
    unsigned char x_pos = 0x00;
    unsigned char page = 0x00;

    for(page = y_min; page < y_max; page++){
        writeCmd((Set_Page_Start_Address_CMD + page), CMD);
        writeCmd(Set_Lower_Column_Start_Address_CMD, CMD);
        writeCmd(Set_Higher_Column_Start_Address_CMD, CMD);

        for(x_pos = x_min; x_pos < x_max; x_pos++) {
            writeCmd(bmp_data, DAT);
        }
    }
} 

void SSD1309::clear_screen(){ 
    clear_buffer();
    show();
}

void SSD1309::clear_buffer(){
    unsigned int s = 0x00;

    for(s = 0; s < buffer_size; s++){
        buffer[s] = 0x00;
    }
}

void SSD1309::setCursor(uint8_t x, uint8_t y){
    cursor_x = x;
    cursor_y = y;
}

void SSD1309::drawPixel(uint8_t x, uint8_t y, uint8_t color){
    unsigned char value = 0x00;
    unsigned char page = 0x00;
    unsigned char bit_pos = 0x00;
    page = (y / y_max);
    bit_pos = (y - (page * y_max));
    value = buffer[((page * x_max) + x)];

    if (color){
        value |= (1 << bit_pos);
    }
    else{
        value &= ~(1 << bit_pos);
    }

    buffer[((page * x_max) + x)] = value;
}

void SSD1309::drawLine(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color){    
    int16_t flat = abs(x1 - x0) > abs(y1 - y0);

    if (flat) {
        _swap_int16_t(x0, y0);
        _swap_int16_t(x1, y1);
    }

    if (y0 > y1) {
        _swap_int16_t(x0, x1);
        _swap_int16_t(y0, y1);
    }

    int16_t dx, dy;
    dx = abs(x1 - x0);
    dy = y1 - y0;

    int16_t err = dy / 2;
    int16_t xstep;

    if (x0 < x1) {
        xstep = 1;
    } else {
        xstep = -1;
    }

    for (; y0<=y1; y0++) {
        if (flat) {
            drawPixel(y0, x0, color);
        } else {
            drawPixel(x0, y0, color);
        }
        err -= dx;
        if (err < 0) {
            x0 += xstep;
            err += dy;
        }
    }
}

void SSD1309::drawFastVLine(uint8_t x, uint8_t y, uint8_t h, uint8_t color){
    drawLine(x, y, x, y+h-1, color);
}

void SSD1309::drawFastHLine(uint8_t x, uint8_t y, uint8_t w, uint8_t color){
    drawLine(x, y, x+w-1, y, color);
}

void SSD1309::drawRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color){
    drawFastHLine(x, y, w, color);
    drawFastHLine(x, y+h-1, w, color);
    drawFastVLine(x, y, h, color);
    drawFastVLine(x+w-1, y, h, color);
}

void SSD1309::fillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color){
    for (uint8_t i=y; i<y+h; i++){
        drawFastHLine(x, i, w, color);
    }
}

void SSD1309::drawCircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color){    
    int16_t f = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x = 0;
    int16_t y = r;

    drawPixel(x0  , y0+r, color);
    drawPixel(x0  , y0-r, color);
    drawPixel(x0+r, y0  , color);
    drawPixel(x0-r, y0  , color);

    while (x<y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        drawPixel(x0 + x, y0 + y, color);
        drawPixel(x0 - x, y0 + y, color);
        drawPixel(x0 + x, y0 - y, color);
        drawPixel(x0 - x, y0 - y, color);
        drawPixel(x0 + y, y0 + x, color);
        drawPixel(x0 - y, y0 + x, color);
        drawPixel(x0 + y, y0 - x, color);
        drawPixel(x0 - y, y0 - x, color);
    }
}

void SSD1309::fillCircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color){
    drawFastVLine(x0, y0-r, 2*r+1, color);
    fillCircleHelper(x0, y0, r, 3, 0, color);
}

void SSD1309::fillCircleHelper(uint8_t x0, uint8_t y0, uint8_t r, uint8_t cornername, uint8_t delta, uint8_t color){
    int16_t f     = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x     = 0;
    int16_t y     = r;

    while (x<y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f     += ddF_y;
        }
        x++;
        ddF_x += 2;
        f     += ddF_x;

        if (cornername & 0x1) {
            drawFastVLine(x0+x, y0-y, 2*y+1+delta, color);
            drawFastVLine(x0+y, y0-x, 2*x+1+delta, color);
        }
        if (cornername & 0x2) {
            drawFastVLine(x0-x, y0-y, 2*y+1+delta, color);
            drawFastVLine(x0-y, y0-x, 2*x+1+delta, color);
        }
    }
}

void SSD1309::setTextAlignRight(uint8_t x){
    align_x = x;
}

void SSD1309::setTextHorizontalCenter(bool c){
    horizontalCenter = c;
}

void SSD1309::setTextColor(uint8_t c){
    textcolor = c;
}

void SSD1309::setFont(const GFXfont *f){
    if(f) {            // Font struct pointer passed in?
        if(!gfxFont) { // And no current font struct?
            // Switching from classic to new font behavior.
            // Move cursor pos down 6 pixels so it's on baseline.
            cursor_y += 6;
        }
    } else if(gfxFont) { // NULL passed.  Current font struct defined?
        // Switching from new to classic font behavior.
        // Move cursor pos up 6 pixels so it's at top-left of char.
        cursor_y -= 6;
    }
    gfxFont = (GFXfont *)f;
}

void SSD1309::drawChar(uint8_t x, uint8_t y, unsigned char c, uint8_t color, uint8_t size){
    if(!gfxFont) { // 'Classic' built-in font
        if(!_cp437 && (c >= 176)) c++; // Handle 'classic' charset behavior

        for(int8_t i=0; i<5; i++ ) { // Char bitmap = 5 columns
            uint8_t line = pgm_read_byte(&font[c * 5 + i]);
            for(int8_t j=0; j<8; j++, line >>= 1) {
                if(line & 1) {
                    if(size == 1)
                        drawPixel(x+i, y+j, color);
                    else
                        fillRect(x+i*size, y+j*size, size, size, color);
                }
            }
        }  
    }
    else { // Custom font 

        // Character is assumed previously filtered by write() to eliminate
        // newlines, returns, non-printable characters, etc.  Calling
        // drawChar() directly with 'bad' characters of font may cause mayhem!

        c -= (uint8_t)pgm_read_byte(&gfxFont->first);
        GFXglyph *glyph  = &(((GFXglyph *)pgm_read_pointer(&gfxFont->glyph))[c]);
        uint8_t  *bitmap = (uint8_t *)pgm_read_pointer(&gfxFont->bitmap);

        uint16_t bo = pgm_read_word(&glyph->bitmapOffset);
        uint8_t  w  = pgm_read_byte(&glyph->width),
                 h  = pgm_read_byte(&glyph->height);
        int8_t   xo = pgm_read_byte(&glyph->xOffset),
                 yo = pgm_read_byte(&glyph->yOffset);
        uint8_t  xx, yy, bits = 0, bit = 0;
        int16_t  xo16 = 0, yo16 = 0;

        if(size > 1) {
            xo16 = xo;
            yo16 = yo;
        }

        // Todo: Add character clipping here

        // NOTE: THERE IS NO 'BACKGROUND' COLOR OPTION ON CUSTOM FONTS.
        // THIS IS ON PURPOSE AND BY DESIGN.  The background color feature
        // has typically been used with the 'classic' font to overwrite old
        // screen contents with new data.  This ONLY works because the
        // characters are a uniform size; it's not a sensible thing to do with
        // proportionally-spaced fonts with glyphs of varying sizes (and that
        // may overlap).  To replace previously-drawn text when using a custom
        // font, use the getTextBounds() function to determine the smallest
        // rectangle encompassing a string, erase the area with fillRect(),
        // then draw new text.  This WILL infortunately 'blink' the text, but
        // is unavoidable.  Drawing 'background' pixels will NOT fix this,
        // only creates a new set of problems.  Have an idea to work around
        // this (a canvas object type for MCUs that can afford the RAM and
        // displays supporting setAddrWindow() and pushColors()), but haven't
        // implemented this yet.

        for(yy=0; yy<h; yy++) {
            for(xx=0; xx<w; xx++) {
                if(!(bit++ & 7)) {
                    bits = pgm_read_byte(&bitmap[bo++]);
                }
                if(bits & 0x80) {
                    if(size == 1) {
                        drawPixel(x+xo+xx, y+yo+yy, color);
                    } else {
                        fillRect(x+(xo16+xx)*size, y+(yo16+yy)*size,
                          size, size, color);
                    }
                }
                bits <<= 1;
            }
        }

    } // End classic vs custom font
}

size_t SSD1309::write(uint8_t c){
    if(!gfxFont) { // 'Classic' built-in font
        //printf("standard font\n");
        if(c == '\n') {                        // Newline?
            cursor_x  = 0;                     // Reset x to zero,
            cursor_y += textsize * 8;          // advance y one line
        } else if(c != '\r') {                 // Ignore carriage returns
            if(wrap && ((cursor_x + textsize * 6) > _width)) { // Off right?
                cursor_x  = 0;                 // Reset x to zero,
                cursor_y += textsize * 8;      // advance y one line
            }
            drawChar(cursor_x, cursor_y, c, textcolor, textsize);
            cursor_x += textsize * 6;          // Advance x one char
        }

    } else { // Custom font
        //printf("custom font\n");
        if(c == '\n') {
            cursor_x  = 0;
            cursor_y += (int16_t)textsize *
                        (uint8_t)pgm_read_byte(&gfxFont->yAdvance);
        } 
        else if(c != '\r' && c != 194) {
            uint8_t first = pgm_read_byte(&gfxFont->first);
            if((c >= first) && (c <= (uint8_t)pgm_read_byte(&gfxFont->last))) {
                GFXglyph *glyph = &(((GFXglyph *)pgm_read_pointer(
                  &gfxFont->glyph))[c - first]);
                uint8_t   w     = pgm_read_byte(&glyph->width),
                          h     = pgm_read_byte(&glyph->height);
                if((w > 0) && (h > 0)) { // Is there an associated bitmap?
                    int16_t xo = (int8_t)pgm_read_byte(&glyph->xOffset); // sic
                    if(wrap && ((cursor_x + textsize * (xo + w)) > _width)) {
                        cursor_x  = 0;
                        cursor_y += (int16_t)textsize *
                          (uint8_t)pgm_read_byte(&gfxFont->yAdvance);
                    }
                    drawChar(cursor_x, cursor_y, c, textcolor, textsize);
                }
                cursor_x += (uint8_t)pgm_read_byte(&glyph->xAdvance) * (int16_t)textsize;
            }
        }

    }
}

uint8_t SSD1309::getCharWidth(char c){
    uint8_t size = 0;
    if(!gfxFont) { // 'Classic' built-in font
        size = textsize * 6;
    }
    else{ // Custom font
        uint8_t first = pgm_read_byte(&gfxFont->first);
        if((c >= first) && (c <= (uint8_t)pgm_read_byte(&gfxFont->last))) {
            GFXglyph *glyph = &(((GFXglyph *)pgm_read_pointer(&gfxFont->glyph))[c - first]);
            size = pgm_read_byte(&glyph->width); 
        }
    }
    return size;
}

uint8_t SSD1309::getCharWidthAndSpace(char c){
    uint8_t size = 0;
    if(!gfxFont) { // 'Classic' built-in font
        size = textsize * 6;
    }
    else{ // Custom font
        uint8_t first = pgm_read_byte(&gfxFont->first);
        if((c >= first) && (c <= (uint8_t)pgm_read_byte(&gfxFont->last))) {
            GFXglyph *glyph = &(((GFXglyph *)pgm_read_pointer(&gfxFont->glyph))[c - first]);
            size = pgm_read_byte(&glyph->xAdvance); 
        }
    }
    return size;
}

void SSD1309::customprint(char *t){
    int i = 0;

    if (horizontalCenter){
        uint8_t width = 0;
        while(t[i] != 0){
            if (t[i+1] != 0){
                width += getCharWidthAndSpace(t[i]);
            }
            else{
                width += getCharWidth(t[i]);
            }
            i++;
        }
        if (width > _width){
            width = _width;
        }
        setCursor((_width/2)-(width/2), cursor_y);
    }
    else if (align_x){
        uint8_t width = 0;
        while(t[i] != 0){
            if (t[i+1] != 0){
                width += getCharWidthAndSpace(t[i]);
            }
            else{
                width += getCharWidth(t[i]);
            }
            i++;
        }
        if (width > _width){
            width = _width;
        }
        if (align_x > width){
            setCursor(align_x - width, cursor_y);
        }
    }

    i = 0;
    
    while(t[i] != 0){
        write(t[i]);
        i++;
    }
}

void SSD1309::drawBitmap(int16_t x, int16_t y, const uint8_t bitmap[], int16_t w, int16_t h) {
    int16_t byteWidth = (w + 7) / 8; // Bitmap scanline pad = whole byte
    uint8_t byte = 0;

    for (int16_t j = 0; j < h; j++, y++) {
        for (int16_t i = 0; i < w; i++) {
            if (i & 7)
                byte <<= 1;
            else
                byte = pgm_read_byte(&bitmap[j * byteWidth + i / 8]);
            drawPixel(x + i, y, (byte & 0x80) ? 1 : 0);
        }
    }
}

void SSD1309::show(void){                                             
    unsigned char x_pos = 0x00;
    unsigned char page = 0x00;
    int i = 0;

    for(page = y_min; page < y_max; page++){
        writeCmd((Set_Page_Start_Address_CMD + page), CMD);
        writeCmd(Set_Lower_Column_Start_Address_CMD, CMD);
        writeCmd(Set_Higher_Column_Start_Address_CMD, CMD);

        for(x_pos = x_min; x_pos < x_max; x_pos++) {
            writeCmd(buffer[i++], DAT);
        }
    }
}

void SSD1309::gpio_init(void){
    gpio_mode_setup(cs_port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, cs_pin);
    gpio_mode_setup(rst_port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, rst_pin);
    gpio_mode_setup(cmd_port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, cmd_pin);
}