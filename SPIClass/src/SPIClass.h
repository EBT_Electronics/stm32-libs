#ifndef SPICLASS_H
#define SPICLASS_H

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>

#define MSBFIRST    0
#define SPI_MODE0   0
#define SS          0

class SPISettings{
    public:
        SPISettings(uint32_t clock, uint8_t bitOrder, uint8_t dataMode) {
            init_AlwaysInline(clock, bitOrder, dataMode);
        }
        SPISettings() {
            init_AlwaysInline(4000000, MSBFIRST, SPI_MODE0);
        }
    private:
        void init_AlwaysInline(uint32_t clock, uint8_t bitOrder, uint8_t dataMode){
            _speed = clock;
            _bitOrder = bitOrder;
            _mode = dataMode;
        }
        uint32_t _speed;
        uint8_t _bitOrder;
        uint8_t _mode;
};

class SPIClass{
    public:
        void begin(uint32_t spiPort);

        void beginTransaction(SPISettings settings);
        void endTransaction();

        uint8_t transfer(uint8_t);
    private:
        uint32_t _spiPort;
};

#endif