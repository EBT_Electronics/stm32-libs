#ifndef _EBT_GFX_H
#define _EBT_GFX_H

#include <Print.h>
#include "gfxfont.h"

#define RGB565_BLACK 0x0000
#define RGB565_WHITE 0xFFFF
#define RGB565_RED 0xF800
#define RGB565_GREEN 0x07E0
#define RGB565_BLUE 0x001F
#define RGB565_CYAN 0x07FF
#define RGB565_MAGENTA 0xF81F
#define RGB565_YELLOW 0xFFE0
#define RGB565_ORANGE 0xFC00

uint16_t RGB888toRGB565(uint32_t color_32);

class EBT_GFX : public Print {
    public:
        EBT_GFX(int16_t w, int16_t h);

        /**********************************************************************/
        /*!
        @brief  Draw to the screen/framebuffer/etc.
        Must be overridden in subclass.
        @param  x    X coordinate in pixels
        @param  y    Y coordinate in pixels
        @param color  16-bit pixel color.
        */
        /**********************************************************************/
        virtual void drawPixel(int16_t x, int16_t y, uint16_t color) = 0;

        // TRANSACTION API / CORE DRAW API
        // These MAY be overridden by the subclass to provide device-specific
        // optimized code.  Otherwise 'generic' versions are used.
        virtual void startWrite(void);
        virtual void writePixel(int16_t x, int16_t y, uint16_t color);
        virtual void writeFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
        virtual void writeFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
        virtual void writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
                                uint16_t color);
        virtual void endWrite(void);

        

        // BASIC DRAW API
        // These MAY be overridden by the subclass to provide device-specific
        // optimized code.  Otherwise 'generic' versions are used.

        // It's good to implement those, even if using transaction API
        virtual void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
        virtual void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
        virtual void fillRect(int16_t x, int16_t y, int16_t w, int16_t h,
                                uint16_t color);
        virtual void fillScreen(uint16_t color);
        // Optional and probably not necessary to change
        virtual void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
                                uint16_t color);
        virtual void drawRect(int16_t x, int16_t y, int16_t w, int16_t h,
                                uint16_t color);

        // These exist only with Adafruit_GFX (no subclass overrides)
        void drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
        void drawCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername,
                                uint16_t color);
        void fillCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
        void fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername,
                                int16_t delta, uint16_t color);
        void drawTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2,
                            int16_t y2, uint16_t color);
        void fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2,
                            int16_t y2, uint16_t color);
        void drawRoundRect(int16_t x, int16_t y, int16_t w, int16_t h,
                            int16_t r, uint16_t color);
        void fillRoundRect(int16_t x, int16_t y, int16_t w, int16_t h,
                            int16_t r, uint16_t color);
        void drawRGBBitmap(int16_t x, int16_t y, const uint16_t bitmap[],
                            int16_t w, int16_t h);
        void drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color,
                        uint16_t bg, uint8_t size);
        void drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color,
                        uint16_t bg, uint8_t size_x, uint8_t size_y);  
        void setTextSize(uint8_t s);
        void setTextSize(uint8_t sx, uint8_t sy);        
        void setFont(const GFXfont *f = NULL);
        /**********************************************************************/
        /*!
            @brief  Set text cursor location
            @param  x    X coordinate in pixels
            @param  y    Y coordinate in pixels
        */
        /**********************************************************************/
        void setCursor(int16_t x, int16_t y) {
            cursor_x = x;
            cursor_y = y;
        }

        /**********************************************************************/
        /*!
            @brief   Set text font color with transparant background
            @param   c   16-bit 5-6-5 Color to draw text with
            @note    For 'transparent' background, background and foreground
                    are set to same color rather than using a separate flag.
        */
        /**********************************************************************/
        void setTextColor(uint16_t c) { textcolor = textbgcolor = c; }

        /**********************************************************************/
        /*!
            @brief   Set text font color with custom background color
            @param   c   16-bit 5-6-5 Color to draw text with
            @param   bg  16-bit 5-6-5 Color to draw background/fill with
        */
        /**********************************************************************/
        void setTextColor(uint16_t c, uint16_t bg) {
            textcolor = c;
            textbgcolor = bg;
        }

        /**********************************************************************/
        /*!
            @brief  Set whether text that is too long for the screen width should
                    automatically wrap around to the next line (else clip right).
            @param  w  true for wrapping, false for clipping
        */
        /**********************************************************************/
        void setTextWrap(bool w) { wrap = w; }
        
        /**********************************************************************/
        /*!
            @brief  Set whether text will be centered horizontally
            @param  h  true for centering, false for aligning left
        */
        /**********************************************************************/
        void setHorizontalCenter(bool h) { horizontalCenter = h; }
        
        /**********************************************************************/
        /*!
            @brief  Set whether text will be aligned right
            @param  x > 0 for aligning right, 0 for aligning left
        */
        /**********************************************************************/
        void setTextAlignRight(uint8_t x) { align_x = x; }
        
        /**********************************************************************/
        /*!
            @brief  Set whether text will be centered vertically
            @param  h  true for centering, false for aligning top
        */
        /**********************************************************************/
        void setVerticalCenter(bool v) { verticalCenter = v; }

        /**********************************************************************/
        /*!
            @brief  Enable (or disable) Code Page 437-compatible charset.
                    There was an error in glcdfont.c for the longest time -- one
                    character (#176, the 'light shade' block) was missing -- this
                    threw off the index of every character that followed it.
                    But a TON of code has been written with the erroneous
                    character indices. By default, the library uses the original
                    'wrong' behavior and old sketches will still work. Pass
                    'true' to this function to use correct CP437 character values
                    in your code.
            @param  x  true = enable (new behavior), false = disable (old behavior)
        */
        /**********************************************************************/
        void cp437(bool x = true) { _cp437 = x; }

        
        virtual size_t write(uint8_t);
        virtual size_t write(const uint8_t *buffer, size_t size);
        using Print::write;
        
        /************************************************************************/
        /*!
            @brief  Get text cursor X location
            @returns    X coordinate in pixels
        */
        /************************************************************************/
        int16_t getCursorX(void) const { return cursor_x; }

        /************************************************************************/
        /*!
            @brief      Get text cursor Y location
            @returns    Y coordinate in pixels
        */
        /************************************************************************/
        int16_t getCursorY(void) const { return cursor_y; };

    protected:
        void charBounds(unsigned char c, int16_t *x, int16_t *y, int16_t *minx,
                    int16_t *miny, int16_t *maxx, int16_t *maxy);
        uint8_t 
            getCharWidth(char c),
            getCharWidthAndSpace(char c);
        int16_t WIDTH;          ///< This is the 'raw' display width - never changes
        int16_t HEIGHT;         ///< This is the 'raw' display height - never changes
        int16_t _width;         ///< Display width as modified by current rotation
        int16_t _height;        ///< Display height as modified by current rotation
        int16_t cursor_x;       ///< x location to start print()ing text
        int16_t cursor_y;       ///< y location to start print()ing text
        uint16_t textcolor;     ///< 16-bit background color for print()
        uint16_t textbgcolor;   ///< 16-bit text color for print()
        uint8_t textsize_x;     ///< Desired magnification in X-axis of text to print()
        uint8_t textsize_y;     ///< Desired magnification in Y-axis of text to print()
        uint8_t rotation;       ///< Display rotation (0 thru 3)
        uint8_t align_x;        ///< align right to this value if > 0
        bool horizontalCenter;  ///< if set, center text horizontally
        bool verticalCenter;    ///< if set, center text vertically
        bool wrap;              ///< If set, 'wrap' text at right edge of display
        bool _cp437;            ///< If set, use correct CP437 charset (default is off)
        GFXfont *gfxFont;       ///< Pointer to special font

};

#endif