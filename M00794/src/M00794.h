#ifndef M00794_H
#define M00794_H

#include <stdint.h>
#include <EBT_GFX.h>

#define OLED_WIDTH  96
#define OLED_HEIGHT 96

class M00794 : public EBT_GFX {
    public:
        M00794(uint32_t wrPort, uint16_t wrPin, 
               uint32_t rdPort, uint16_t rdPin, 
               uint32_t csPort, uint16_t csPin, 
               uint32_t rstPort, uint16_t rstPin,
               uint32_t a0Port, uint16_t a0Pin,
               uint32_t d0Port, uint16_t d0Pin,
               uint32_t d1Port, uint16_t d1Pin,
               uint32_t d2Port, uint16_t d2Pin,
               uint32_t d3Port, uint16_t d3Pin,
               uint32_t d4Port, uint16_t d4Pin,
               uint32_t d5Port, uint16_t d5Pin,
               uint32_t d6Port, uint16_t d6Pin,
               uint32_t d7Port, uint16_t d7Pin
        );

        void setupIO(void);
		void setRotation(uint8_t r);
		void setPeakCurrent(uint8_t c);

        void init(void);

        virtual void drawPixel(int16_t x, int16_t y, uint16_t color);
        virtual void fillScreen(uint16_t color);

        void show();

    private:
        void writeDisplayData(uint8_t dat);
        void write_com(uint8_t com);
        void write_dat(uint8_t dat);

        uint32_t _wrPort, _rdPort, _csPort, _rstPort, _a0Port, _d0Port, _d1Port, _d2Port, _d3Port, _d4Port, _d5Port, _d6Port, _d7Port;
        uint16_t _wrPin, _rdPin, _csPin, _rstPin, _a0Pin, _d0Pin, _d1Pin, _d2Pin, _d3Pin, _d4Pin, _d5Pin, _d6Pin, _d7Pin;

        uint8_t
            rotation,
			peakcurrent;

        uint16_t frameBuffer[9216];
};

#endif