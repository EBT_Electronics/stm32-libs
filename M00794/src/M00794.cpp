#include <M00794.h>

#include <libopencm3/stm32/gpio.h>

#include <delay.h>

M00794::M00794(uint32_t wrPort, uint16_t wrPin, 
               uint32_t rdPort, uint16_t rdPin, 
               uint32_t csPort, uint16_t csPin, 
               uint32_t rstPort, uint16_t rstPin,
               uint32_t a0Port, uint16_t a0Pin,
               uint32_t d0Port, uint16_t d0Pin,
               uint32_t d1Port, uint16_t d1Pin,
               uint32_t d2Port, uint16_t d2Pin,
               uint32_t d3Port, uint16_t d3Pin,
               uint32_t d4Port, uint16_t d4Pin,
               uint32_t d5Port, uint16_t d5Pin,
               uint32_t d6Port, uint16_t d6Pin,
               uint32_t d7Port, uint16_t d7Pin) : 
        _wrPort(wrPort), 
        _rdPort(rdPort), 
        _csPort(csPort), 
        _rstPort(rstPort), 
        _a0Port(a0Port), 
        _d0Port(d0Port), 
        _d1Port(d1Port), 
        _d2Port(d2Port), 
        _d3Port(d3Port), 
        _d4Port(d4Port), 
        _d5Port(d5Port), 
        _d6Port(d6Port), 
        _d7Port(d7Port),
        _wrPin(wrPin),
        _rdPin(rdPin), 
        _csPin(csPin),
        _rstPin(rstPin),
        _a0Pin(a0Pin),
        _d0Pin(d0Pin),
        _d1Pin(d1Pin),
        _d2Pin(d2Pin),
        _d3Pin(d3Pin),
        _d4Pin(d4Pin),
        _d5Pin(d5Pin),
        _d6Pin(d6Pin),
        _d7Pin(d7Pin), 
        EBT_GFX(OLED_WIDTH, OLED_HEIGHT){
    // fill_n(frameBuffer, 9216, 0)
}

void M00794::setupIO(void){
    gpio_mode_setup(_wrPort, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _wrPin);  
    gpio_mode_setup(_rdPort, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _rdPin);  
    gpio_mode_setup(_csPort, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _csPin);  
    gpio_mode_setup(_rstPort, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _rstPin);  
    gpio_mode_setup(_a0Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _a0Pin);  
    gpio_mode_setup(_d0Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d0Pin);   
    gpio_mode_setup(_d1Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d1Pin);   
    gpio_mode_setup(_d2Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d2Pin);   
    gpio_mode_setup(_d3Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d3Pin);   
    gpio_mode_setup(_d4Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d4Pin);   
    gpio_mode_setup(_d5Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d5Pin);   
    gpio_mode_setup(_d6Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d6Pin);   
    gpio_mode_setup(_d7Port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, _d7Pin);  

    gpio_set(_wrPort, _wrPin); 
    gpio_set(_rdPort, _rdPin);
    gpio_set(_csPort, _csPin);
    gpio_set(_rstPort, _rstPin);
    gpio_clear(_a0Port, _a0Pin);
    gpio_clear(_d0Port, _d0Pin); 
    gpio_clear(_d1Port, _d1Pin);  
    gpio_clear(_d2Port, _d2Pin);  
    gpio_clear(_d3Port, _d3Pin);  
    gpio_clear(_d4Port, _d4Pin);  
    gpio_clear(_d5Port, _d5Pin);  
    gpio_clear(_d6Port, _d6Pin);  
    gpio_clear(_d7Port, _d7Pin); 
}

// Sets the rotation of the screen
// 1 is down, 2 is up, 4 is left, 7 is right
// has to be called BEFORE init funtion
void M00794::setRotation(uint8_t r){
	rotation = r;

}
void M00794::setPeakCurrent(uint8_t c){
	peakcurrent = c;
}

void M00794::init(void){  
    gpio_set(_csPort, _csPin);
    gpio_set(_a0Port, _a0Pin);
    gpio_set(_wrPort, _wrPin); 

    gpio_clear(_rstPort, _rstPin);
    delay(50);
    gpio_set(_rstPort, _rstPin);

    write_com(0x02);
    write_dat(0x00);        //turn OFF

    write_com(0x01);        //software reset The OSC.is stopped.

    write_com(0x03);
    write_dat(0x00);        //Ste Dot Matrix Display Stand-by OFF

    write_com(0x04);        //Set OSC Control
    write_dat(0x03);        //105Hz

    write_com(0x05);        //Set Graphic RAM Writing Direction
	write_dat(rotation);	//RGB	1 = down, 2 = up, 4 = left, 7 = right

    write_com(0x06);        //Set Row Scan Direction
    write_dat(0x01);        //0x01..0x00

    write_com(0x07);        //Set Diplay Size

    write_dat(0x00);        //X start 1
    write_dat(0x00);        //X start 2

    write_dat(0x04);        //X end 1
    write_dat(0x1f);        //X end 2

    write_dat(0x00);        //Y start 1
    write_dat(0x00);        //Y start 2

    write_dat(0x05);        //Y end 1
    write_dat(0x0f);        //Y end 2

    write_com(0x08);        //Set Interface Bus Type
    write_dat(0x01);        //8Bit I/F Bus

    write_com(0x09);        //Set Masking Data
    write_dat(0x07);        //Data AND Palette(R,G,B) -->Output Data

    write_com(0x0a);        //Set Read/Write Box Data

    write_dat(0x00);        //X start 1
    write_dat(0x00);        //X start 2

    write_dat(0x05);        //X end 1
    write_dat(0x0f);        //X end 2

    write_dat(0x00);        //Y start 1
    write_dat(0x00);        //Y start 2

    write_dat(0x05);        //Y end 1
    write_dat(0x0f);        //Y end 2

    write_com(0x0b);        //Set Display Start Address

    write_dat(0x00);        //1st Parameter
    write_dat(0x00);        //2st Parameter

    write_dat(0x00);        //3st Parameter
    write_dat(0x00);        //4st Parameter

    write_com(0x0e);        //Set Dot Matrix Current Level (0-255uA)

    write_dat(0x03);        //1st Paramenter	R[7:4]
    write_dat(0x0F);        //2st Paramenter	R[3:0]

    write_dat(0x02);        //3st Paramenter	G[7:4]
    write_dat(0x04);        //4st Paramenter	G[3:0]

    write_dat(0x03);        //5st Paramenter	B[7:4]
    write_dat(0x08);        //6st Paramenter	B[3:0]  

    write_com(0x0f);        //Set Dot Matrix Peak Current Level (0-1008uA)

    write_dat(0xa);        //PR[5:0]        16uA Step
    write_dat(0xa);        //PG[5:0]        16uA Step
    write_dat(0xa);        //PB[5:0]        16uA Step

    write_com(0x1c);        //Set Pre-Charge Width
    write_dat(0x08);        //0x08        //Parameter Range :01h-3fh

    write_com(0x1d);        //Set Peak Pulse Width;Parameter Range :01h-3fh

//  write_dat(0x00);        //for Red
//  write_dat(0x00);        //for Green
//  write_dat(0x00);        //for Blue
    write_dat(0);        //for Red
    write_dat(0);        //for Green
    write_dat(0);        //for Blue

    write_com(0x1e);        //Set Peak Pulse Delay
    write_dat(0x05);        //01h-0fh

    write_com(0x1f);        //Set Row Scan Operation
    write_dat(0x00);        //0x00..0x10

    write_com(0x30);        //Set Internal Regulator for Row Scan
    write_dat(0x10);        //VCC_R =0.85 VCC_C

    write_com(0x3b);        //Set Gamma Correction Table Initialize

    write_com(0x3c);        //Set VDD Selection
    write_dat(0x00);        //VDD=2.8V; 0x01:VDD=1.8V

    write_com(0x3d);        //Set DMODE Selection
    write_dat(0x00);        //Resolution=96*96,65k Color


    write_com(0x02);        //Original 0x02
    write_dat(0x01);        //turn ON
}

void M00794::drawPixel(int16_t x, int16_t y, uint16_t color){
    frameBuffer[y * 96 + x] = color;
}

void M00794::fillScreen(uint16_t color){
    for (int i = 0; i < 9216; i++){
        frameBuffer[i] = color;
    }
}

void M00794::show(){    
    write_com(0x0b);        //Set Diplay Start Address

    write_dat(0x00);        //1st Parameter
    write_dat(0x00);        //2st Parameter

    write_dat(0x00);        //3st Parameter
    write_dat(0x00);        //4st Parameter

    write_com(0x0c);        //Read/Write Dot matrix Display Data
    for (int i = 0; i < 9216; i++){
        write_dat(frameBuffer[i] >> 8);
        write_dat(frameBuffer[i] & 0x00FF);
    }
}

void M00794::writeDisplayData(uint8_t dat){
    (dat & 0x01) ? (gpio_set(_d0Port, _d0Pin)):(gpio_clear(_d0Port, _d0Pin));
    (dat & 0x02) ? (gpio_set(_d1Port, _d1Pin)):(gpio_clear(_d1Port, _d1Pin));
    (dat & 0x04) ? (gpio_set(_d2Port, _d2Pin)):(gpio_clear(_d2Port, _d2Pin));
    (dat & 0x08) ? (gpio_set(_d3Port, _d3Pin)):(gpio_clear(_d3Port, _d3Pin));
    (dat & 0x10) ? (gpio_set(_d4Port, _d4Pin)):(gpio_clear(_d4Port, _d4Pin));
    (dat & 0x20) ? (gpio_set(_d5Port, _d5Pin)):(gpio_clear(_d5Port, _d5Pin));
    (dat & 0x40) ? (gpio_set(_d6Port, _d6Pin)):(gpio_clear(_d6Port, _d6Pin));
    (dat & 0x80) ? (gpio_set(_d7Port, _d7Pin)):(gpio_clear(_d7Port, _d7Pin));
}

void M00794::write_com(uint8_t com){
    gpio_clear(_a0Port, _a0Pin);
    gpio_clear(_csPort, _csPin);

    writeDisplayData(com);

    gpio_clear(_wrPort, _wrPin);
    gpio_set(_wrPort, _wrPin);
    gpio_set(_csPort, _csPin);
}

void M00794::write_dat(uint8_t dat){
    gpio_set(_a0Port, _a0Pin);
    gpio_clear(_csPort, _csPin);
    gpio_set(_rdPort, _rdPin);         // disable read (active low)
    gpio_clear(_wrPort, _wrPin);       // enable write (active low)

    writeDisplayData(dat);

    gpio_set(_rdPort, _rdPin);         // disable read (active low)
    gpio_set(_wrPort, _wrPin);         // disable write (active low)
    gpio_set(_csPort, _csPin);
}