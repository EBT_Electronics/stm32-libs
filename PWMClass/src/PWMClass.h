#ifndef _PWMCLASS_H
#define _PWMCLASS_H

class PWMClass{
    public:
        virtual void init(uint32_t freq) = 0;
        virtual void setDuty(uint8_t duty) = 0;
        virtual void start() = 0;
        virtual void stop() = 0;
};

#endif