#include <customUSART.h>
#include <delay.h>
#include <stdio.h>
#include <libopencm3/stm32/usart.h>

uint32_t counter;

void usart_wait_transmission_complete(uint32_t usart){
    #if defined(STM32F0)
    while ((USART_ISR(usart) & USART_ISR_TC) == 0);
    #else
    while ((USART_SR(usart) & USART_SR_TC) == 0);
    #endif
}

bool usart_recv_ready(uint32_t usart){
    #if defined(STM32F0)
    return (USART_ISR(usart) & USART_ISR_RXNE);
    #else
    return (USART_SR(usart) & USART_SR_RXNE);
    #endif
}

int8_t usart_wait_recv_ready_timeOut(uint32_t usart, uint32_t timeOut){
    counter = millis();

    while((usart_recv_ready(usart) == 0) && ((millis() - counter) <= timeOut));
    
    if(((millis() - counter) > timeOut)){
        return -1;
    }
    return 0;
}