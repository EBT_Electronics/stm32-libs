#ifndef CUSTOMUSART_H
#define CUSTOMUSART_H

#include <stdint.h>

void usart_wait_transmission_complete(uint32_t usart);
bool usart_recv_ready(uint32_t usart);
int8_t usart_wait_recv_ready_timeOut(uint32_t usart, uint32_t timeOut);

#endif