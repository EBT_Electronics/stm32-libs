#ifndef USARTCLASS_H
#define USARTCLASS_H

#include <Stream.h> 

#define RX_BUFFER_SIZE  255

class UsartClass : public Stream{
    protected:
        volatile uint8_t _rx_buffer_head = 0;
        volatile uint8_t _rx_buffer_tail = 0;

        uint8_t _rx_buffer[RX_BUFFER_SIZE];
    public:
        UsartClass(uint32_t usartPort);
        void begin(unsigned long baud);
        void end(void);
        virtual int available(void);
        virtual int peek(void);
        virtual int read(void);
        int timedRead(void);
        int timedPeek(void);
        virtual int availableForWrite(void);
        virtual void flush(void);
        void waitTransmissionComplete(void);
        virtual size_t write(uint8_t);
        inline size_t write(unsigned long n) { return write((uint8_t)n); }
        inline size_t write(long n) { return write((uint8_t)n); }
        inline size_t write(unsigned int n) { return write((uint8_t)n); }
        inline size_t write(int n) { return write((uint8_t)n); }
        using Print::write; // pull in write(str) and write(buf, size) from Print

        // Interrupt handlers
        void rx_complete_irq(void);

    private:
        uint32_t _usartPort;
};

#endif