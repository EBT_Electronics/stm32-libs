#include <UsartClass.h>

#include <stdint.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/nvic.h>

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>

#include <delay.h>

extern void usart_init(uint32_t up, unsigned long br);

UsartClass::UsartClass(uint32_t usartPort){
    _usartPort = usartPort;
}

void UsartClass::begin(unsigned long baudrate){
    usart_init(_usartPort, baudrate);
}

void UsartClass::end(void){
    usart_disable(_usartPort);
}

int UsartClass::available(void){
    return ((unsigned int)(RX_BUFFER_SIZE + _rx_buffer_head - _rx_buffer_tail)) % RX_BUFFER_SIZE;
}

int UsartClass::peek(void){
    if (_rx_buffer_head == _rx_buffer_tail) {
        return -1;
    } else {
        return _rx_buffer[_rx_buffer_tail];
    }
}

int UsartClass::read(void){  
    // if the head isn't ahead of the tail, we don't have any characters
    if (_rx_buffer_head == _rx_buffer_tail) {
        return -1;
    } else {
        unsigned char c = _rx_buffer[_rx_buffer_tail];
        _rx_buffer_tail = (uint8_t)(_rx_buffer_tail + 1) % RX_BUFFER_SIZE;
        return c;
    }
}

int UsartClass::timedRead()
{
  int c;
  _startMillis = millis();
  do {
    c = read();
    if (c >= 0) return c;
  } while(millis() - _startMillis < _timeout);
  return -1;     // -1 indicates timeout
}

int UsartClass::timedPeek()
{
  int c;
  _startMillis = millis();
  do {
    c = peek();
    if (c >= 0) return c;
  } while(millis() - _startMillis < _timeout);
  return -1;     // -1 indicates timeout
}

int UsartClass::availableForWrite(void){

}

void UsartClass::flush(void){
    _rx_buffer_head = 0;
    _rx_buffer_tail = 0;
}

void UsartClass::waitTransmissionComplete(void){
    #if defined(STM32F3) || defined(STM32L0)
        while ((USART_ISR(_usartPort) & USART_ISR_TC) == 0);
    #else
        while ((USART_SR(_usartPort) & USART_SR_TC) == 0);
    #endif
}

size_t UsartClass::write(uint8_t c){
    usart_send_blocking(_usartPort, c);
}


void UsartClass::rx_complete_irq(void){
    #if defined(STM32F3) || defined(STM32L0)
        /* Check if we were called because of RXNE. */
        if (((USART_CR1(_usartPort) & USART_CR1_RXNEIE) != 0) &&
            ((USART_ISR(_usartPort) & USART_ISR_RXNE) != 0)) {

            /* Retrieve the data from the peripheral. */
            uint8_t c = usart_recv(_usartPort);
            uint8_t i = (_rx_buffer_head + 1) % RX_BUFFER_SIZE;

            if (i != _rx_buffer_tail){
                _rx_buffer[_rx_buffer_head] = c;
                _rx_buffer_head = i;
            }
        }
    #else
        /* Check if we were called because of RXNE. */
        if (((USART_CR1(_usartPort) & USART_CR1_RXNEIE) != 0) &&
            ((USART_SR(_usartPort) & USART_SR_RXNE) != 0)) {

            /* Retrieve the data from the peripheral. */
            uint8_t c = usart_recv(_usartPort);
            uint8_t i = (_rx_buffer_head + 1) % RX_BUFFER_SIZE;

            if (i != _rx_buffer_tail){
                _rx_buffer[_rx_buffer_head] = c;
                _rx_buffer_head = i;
            }
        }
    #endif
}