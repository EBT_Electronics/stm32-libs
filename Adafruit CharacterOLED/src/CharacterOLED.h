#ifndef Adafruit_CharacterOLED_h
#define Adafruit_CharacterOLED_h

#include <inttypes.h>
#include "Print.h"

// OLED hardware versions
#define OLED_V1 0x01
#define OLED_V2 0x02

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x28
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE    0x10
#define LCD_4BITMODE    0x00
#define LCD_JAPANESE    0x00
#define LCD_EUROPEAN_I  0x01
#define LCD_RUSSIAN     0x02
#define LCD_EUROPEAN_II 0x03

class Adafruit_CharacterOLED : public Print {
public:  
  void init(uint8_t ver, 
                uint32_t rsPort, uint16_t rsPin,
                uint32_t rwPort, uint16_t rwPin, 
                uint32_t enablePort, uint16_t enablePin,
		        uint32_t d4Port, uint16_t d4Pin, 
                uint32_t d5Port, uint16_t d5Pin,
                uint32_t d6Port, uint16_t d6Pin, 
                uint32_t d7Port, uint16_t d7Pin);
    
  void begin(uint8_t cols, uint8_t rows);

  void clear();
  void home();

//   void print(unsigned char row, unsigned char col, char *t);

  void noDisplay();
  void display();
  void noBlink();
  void blink();
  void noCursor();
  void cursor();
  void scrollDisplayLeft();
  void scrollDisplayRight();
  void leftToRight();
  void rightToLeft();
  void autoscroll();
  void noAutoscroll();

  void createChar(uint8_t, uint8_t[]);
  void setCursor(uint8_t, uint8_t); 
  virtual size_t write(uint8_t);
  void command(uint8_t);

  
  const char *emptyLine = "                    ";
  
private:
  void send(uint8_t, uint8_t);
  void write4bits(uint8_t);
  void pulseEnable();
  void waitForReady();

  uint8_t _oled_ver; // OLED_V1 = older, OLED_V2 = newer hardware version.
    uint32_t 
        _rs_port,
        _rw_port,
        _enable_port,
        _busy_port,
        _data_ports[4];

    uint16_t 
        _rs_pin,        // LOW: command.  HIGH: character.
        _rw_pin,        // LOW: write to LCD.  HIGH: read from LCD.
        _enable_pin,    // activated by a HIGH pulse.
        _busy_pin,      // HIGH means not ready for next command
        _data_pins[4];

  uint8_t _displayfunction;
  uint8_t _displaycontrol;
  uint8_t _displaymode;
  uint8_t _initialized;
  uint8_t _currline;
  uint8_t _numlines;
};

extern Adafruit_CharacterOLED oled;

#endif