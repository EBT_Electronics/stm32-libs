#include <M0041F.h>

#include <libopencm3/stm32/gpio.h>

#include <delay.h>

M0041F::M0041F(uint32_t dcPort, uint16_t dcPin, uint32_t csPort, uint16_t csPin, uint32_t rstPort, uint16_t rstPin, int16_t w, int16_t h):
    EBT_GFX(w, h),
    _dcPort(dcPort),
    _csPort(csPort),
    _rstPort(rstPort),
    _dcPin(dcPin),
    _csPin(csPin),
    _rstPin(rstPin){

}

void M0041F::init(void){
    gpio_set(_rstPort, _rstPin);

    delay(10);

    gpio_clear(_rstPort, _rstPin);

    delay(50);

    gpio_set(_rstPort, _rstPin);

    writeCommand(0xFD); //Set Command Lock
    writeCommand(0x12);
    writeCommand(0xAE); //Set Display OFF
    writeCommand(0x15); //Set Column Address
    writeCommand(0x00);
    writeCommand(0x7F);
    writeCommand(0x75); //Set Row Address
    writeCommand(0x00);
    writeCommand(0x1F);
    writeCommand(0x81); //Set Contrast Current
    writeCommand(0x27);
    writeCommand(0x87); //Set Current Range
    writeCommand(0xA0); //Set Re-map and Gray Scale /Mono Mode
    writeCommand(0x07);
    writeCommand(0xA1); //Set Display Start Line
    writeCommand(0x00);
    writeCommand(0xA2); //Set Display Offset
    writeCommand(0x00);
    writeCommand(0xA8); //Set MUX Ratio
    writeCommand(0x1F);
    writeCommand(0xB1); //Set Phase Length
    writeCommand(0x71);
    writeCommand(0xB3); //Set Front Clock Divider / Oscillator
    writeCommand(0xF0);
    writeCommand(0xB7); //Select Default Linear Gray Scale table
    writeCommand(0xBB); //Set Pre-charge Setup
    writeCommand(0x35);
    writeCommand(0xFF);
    writeCommand(0xBC); //Set Pre-charge voltage
    writeCommand(0x1F);
    writeCommand(0xBE); //Set VCOMH
    writeCommand(0x0F);
    fillScreen(0x00);
    writeCommand(0xAF); //Set Display ON
}

void M0041F::writeCommand(uint8_t cmd){
    gpio_clear(_csPort, _csPin);
    gpio_clear(_dcPort, _dcPin);

    M0041F_spi_out(cmd);

    gpio_set(_dcPort, _dcPin);
    gpio_set(_csPort, _csPin);

}

void M0041F::writeData(uint8_t dat){
    gpio_clear(_csPort, _csPin);

    M0041F_spi_out(dat);

    gpio_set(_csPort, _csPin);

}

void M0041F::drawPixel(int16_t x, int16_t y, uint16_t color){
    // subtract y from 31 because disp need te be inverted
    frameBuffer[(31-y)*256 + x] = color;
}

void M0041F::fillScreen(uint8_t c){
    for (int i = 0; i < 8192; i++){
        frameBuffer[i] = c;
    }
}

void M0041F::show(){
    uint8_t frame;
    gpio_clear(_csPort, _csPin);
    for (int i = 0; i < 8192; i+=2){
        frame = frameBuffer[i];
        frame |= frameBuffer[i+1] << 4;
        M0041F_spi_out(frame);
    }
    gpio_set(_csPort, _csPin);
}
