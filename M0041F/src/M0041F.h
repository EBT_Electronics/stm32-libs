#ifndef M0041F_H
#define M0041F_H

#include <EBT_GFX.h>

extern void M0041F_spi_out(uint8_t d);

class M0041F : public EBT_GFX {
    public:
        M0041F(uint32_t dcPort, uint16_t dcPin, uint32_t csPort, uint16_t csPin, uint32_t rstPort, uint16_t rstPin, int16_t w, int16_t h);

        void init(void);

        void writeCommand(uint8_t cmd);
        void writeData(uint8_t dat);

        // Overloaded functions
        void drawPixel(int16_t x, int16_t y, uint16_t color);

        void fillScreen(uint8_t c);

        void show();

    private:
        uint32_t _dcPort,
                _csPort,
                _rstPort;
        uint16_t _dcPin,
                _csPin,
                _rstPin;

        uint8_t frameBuffer[8192];  // 4 bits per pixel

};

#endif