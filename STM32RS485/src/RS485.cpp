#include "RS485.h"

#include <delay.h>

extern void gpio_set_input(uint32_t gpioPort, uint16_t gpioPin);
extern void gpio_set_output(uint32_t gpioPort, uint16_t gpioPin);

void RS485Class::setup(UsartClass& serial, uint32_t deport, uint16_t depin, uint32_t report, uint16_t repin){
    _serial = &serial;
    _dePort = deport;
    _dePin = depin;
    _rePort = report;
    _rePin = repin;
}

void RS485Class::setup(UsartClass& serial, uint32_t deport, uint16_t depin){
    setup(serial, deport, depin, deport, depin);
}

void RS485Class::begin(unsigned long baudrate, uint16_t config){
    //usart_rs485_setup(baudrate);
    _serial->begin(baudrate);
    gpio_set_output(_dePort, _dePin);
    gpio_set_output(_rePort, _rePin);
    gpio_clear(_dePort, _dePin);
    gpio_set(_rePort, _rePin);
}

void RS485Class::end(){
    _serial->end();
    gpio_clear(_dePort, _dePin);
    gpio_set_input(_dePort, _dePin);
}

int RS485Class::peek(){
    return _serial->peek();
}

int RS485Class::available(){
    return _serial->available();
}

int RS485Class::read(void){
    return _serial->read();
}

int RS485Class::timedRead(){
  int c;
  uint32_t _startMillis = millis();
  do {
    c = read();
    if (c >= 0) return c;
  } while(millis() - _startMillis < _timeout);
  return -1;     // -1 indicates timeout
}

size_t RS485Class::readBytes(uint8_t *buffer, size_t length){
  size_t count = 0;
  while (count < length) {
    int c = timedRead();
    if (c < 0) break;
    *buffer++ = (char)c;
    count++;
  }
  return count;
}

size_t RS485Class::write(uint8_t b){
    _serial->write(b);
    return 1;
}

void RS485Class::beginTransmission(){
    gpio_set(_dePort, _dePin);
    delayMicroseconds(10);

    _transmisionBegun = true;
}

void RS485Class::endTransmission(){
//   _serial->flush();

    _serial->waitTransmissionComplete();

    delayMicroseconds(10);
    gpio_clear(_dePort, _dePin);

    _transmisionBegun = false;
}

void RS485Class::receive(){
    gpio_clear(_rePort, _rePin);
}

void RS485Class::noReceive(){
    gpio_set(_rePort, _rePin);
}

RS485Class RS485;
