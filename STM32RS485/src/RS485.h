#ifndef _RS485_H_INCLUDED
#define _RS485_H_INCLUDED

#include <stdint.h>
#include <Stream.h>

#include <UsartClass.h>

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>

#define SERIAL_8N1  0x06

class RS485Class : public Stream{
    public:
        void setup(UsartClass& serial, uint32_t deport, uint16_t depin);
        void setup(UsartClass& serial, uint32_t deport, uint16_t depin, uint32_t report, uint16_t repin);
        void begin(unsigned long baudrate, uint16_t config = SERIAL_8N1);
        void end();
        virtual int available();
        virtual int peek();
        int read(void);
        size_t readBytes(uint8_t *buffer, size_t length);
        size_t write(uint8_t b);
        using Print::write; // pull in write(str) and write(buf, size) from Print

        void beginTransmission();
        void endTransmission();
        void receive();
        void noReceive();

    protected:
        int timedRead();

    private:
        UsartClass* _serial;
        uint32_t 
            _dePort,
            _rePort;
        uint16_t
            _dePin,
            _rePin;

        unsigned long _baudrate;
        
        bool _transmisionBegun;
        uint32_t _timeout = 1000;
};

extern RS485Class RS485;

#endif