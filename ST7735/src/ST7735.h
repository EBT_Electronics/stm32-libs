#ifndef ST7735_H
#define ST7735_H

#include <stdint.h>

#include <PWMClass.h>

#include <EBT_GFX.h>

#define INITR_MINI160x80 0x04

#define ST_CMD_DELAY    0x80 // special signifier for command lists

#define ST7735_TFTWIDTH  128
#define ST7735_TFTHEIGHT 160

#define ST7735_NOP     0x00
#define ST7735_SWRESET 0x01
#define ST7735_RDDID   0x04
#define ST7735_RDDST   0x09

#define ST7735_SLPIN   0x10
#define ST7735_SLPOUT  0x11
#define ST7735_PTLON   0x12
#define ST7735_NORON   0x13

#define ST7735_INVOFF  0x20
#define ST7735_INVON   0x21
#define ST7735_DISPOFF 0x28
#define ST7735_DISPON  0x29
#define ST7735_CASET   0x2A
#define ST7735_RASET   0x2B
#define ST7735_RAMWR   0x2C
#define ST7735_RAMRD   0x2E

#define ST7735_PTLAR   0x30
#define ST7735_COLMOD  0x3A
#define ST7735_MADCTL  0x36

#define ST7735_FRMCTR1 0xB1
#define ST7735_FRMCTR2 0xB2
#define ST7735_FRMCTR3 0xB3
#define ST7735_INVCTR  0xB4
#define ST7735_DISSET5 0xB6

#define ST7735_PWCTR1  0xC0
#define ST7735_PWCTR2  0xC1
#define ST7735_PWCTR3  0xC2
#define ST7735_PWCTR4  0xC3
#define ST7735_PWCTR5  0xC4
#define ST7735_VMCTR1  0xC5

#define ST7735_RDID1   0xDA
#define ST7735_RDID2   0xDB
#define ST7735_RDID3   0xDC
#define ST7735_RDID4   0xDD

#define ST7735_PWCTR6  0xFC

#define ST7735_GMCTRP1 0xE0
#define ST7735_GMCTRN1 0xE1

#define ST77XX_MADCTL_MY 0x80
#define ST77XX_MADCTL_MX 0x40
#define ST77XX_MADCTL_MV 0x20
#define ST77XX_MADCTL_ML 0x10
#define ST77XX_MADCTL_RGB 0x08

extern void ST7735_spi_out(uint8_t d);

class ST7735 : public EBT_GFX {
    public:
        ST7735(uint32_t dcPort, uint16_t dcPin, uint32_t csPort, uint16_t csPin, uint32_t rstPort, uint16_t rstPin, uint32_t blPort, uint16_t blPin, int16_t w, int16_t h);
        ST7735(uint32_t dcPort, uint16_t dcPin, uint32_t csPort, uint16_t csPin, uint32_t rstPort, uint16_t rstPin, uint32_t blPort, uint16_t blPin, int16_t w, int16_t h, PWMClass* pwm);
        
        void init(void);
        void reset(void);

        void backLightOn();
        void backLightOff();

        // PWM functions
        void setDuty(uint8_t duty);

        // DISP functions
        void startWrite(void),
            endWrite(void),
            setRotation(uint8_t r),
            sendCommand(uint8_t commandByte, const uint8_t *dataBytes, uint8_t numDataBytes),
            writeCommand(uint8_t c),
            writeData(uint8_t d),
            SPI_WRITE16(uint16_t w),
            SPI_WRITE32(uint32_t l),
            setAddrWindow(uint16_t x, uint16_t y, uint16_t w, uint16_t h),
            drawPixel(int16_t x, int16_t y, uint16_t color),
            writePixel(int16_t x, int16_t y, uint16_t color),
            show(void),
            show(uint16_t x, uint16_t y, uint16_t w, uint16_t h);

        // DRAW FUNCTIONS (overload from EBT_GFX)
            void fillScreen(uint16_t color);

    private:
        void displayInit(const uint8_t *addr);

        PWMClass* _pwm;

        uint32_t _dcPort,
                _csPort,
                _rstPort,
                _blPort;
        uint16_t _dcPin,
                _csPin,
                _rstPin,
                _blPin;

        uint16_t frameBuffer[12800] = {0};

        uint8_t tabcolor;

        uint8_t  addr_row, addr_col;

        uint8_t _colstart = 0,      ///< Some displays need this changed to offset
                _rowstart = 0;      ///< Some displays need this changed to offset
        int16_t _xstart = 0;          ///< Internal framebuffer X offset
        int16_t _ystart = 0;          ///< Internal framebuffer Y offset

        uint8_t 
                rotation;

        int16_t  _width, _height, // Display w/h as modified by current rotation
                cursor_x, cursor_y, padX;
};

#endif