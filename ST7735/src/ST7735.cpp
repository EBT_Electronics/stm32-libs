#include "ST7735.h"

#include <delay.h>
#include <pgmspace.h>

#include <libopencm3/stm32/gpio.h>

static const uint8_t PROGMEM
    Rcmd1[] = {                         // 7735R init, part 1 (red or green tab)
        15,                                 // 15 commands in list:
        ST7735_SWRESET,   ST_CMD_DELAY,     //  1: Software reset, 0 args, w/delay
            150,                            //     150 ms delay
        ST7735_SLPOUT,    ST_CMD_DELAY,     //  2: Out of sleep mode, 0 args, w/delay
            255,                            //     500 ms delay
        ST7735_FRMCTR1, 3,                  //  3: Framerate ctrl - normal mode, 3 arg:
            0x02, 0x2C, 0x2D,               //     Rate = fosc/(1 + 20) * (LINE+2C+2D)
        ST7735_FRMCTR2, 3,                  //  4: Framerate ctrl - idle mode, 3 args:
            0x02, 0x2C, 0x2D,               //     Rate = fosc/(1x2+40) * (LINE+2C+2D)
        ST7735_FRMCTR3, 6,                  //  5: Framerate - partial mode, 6 args:
            0x02, 0x2C, 0x2D,               //     Dot inversion mode
            0x02, 0x2C, 0x2D,               //     Line inversion mode
        ST7735_INVCTR,  1,                  //  6: Display inversion ctrl, 1 arg:
            0x07,                           //     No inversion
        ST7735_PWCTR1,  3,                  //  7: Power control, 3 args, no delay:
            0xA2,
            0x02,                           //     -4.6V
            0x84,                           //     AUTO mode
        ST7735_PWCTR2,  1,                  //  8: Power control, 1 arg, no delay:
            0xC5,                           //     VGH25=2.4C VGSEL=-10 VGH=3 * AVDD
        ST7735_PWCTR3,  2,                  //  9: Power control, 2 args, no delay:
            0x0A,                           //     Opamp current small
            0x00,                           //     Boost frequency
        ST7735_PWCTR4,  2,                  // 10: Power control, 2 args, no delay:
            0x8A,                           //     BCLK/2,
            0x2A,                           //     opamp current small & medium low
        ST7735_PWCTR5,  2,                  // 11: Power control, 2 args, no delay:
            0x8A, 0xEE,
        ST7735_VMCTR1,  1,                  // 12: Power control, 1 arg, no delay:
            0x0E,
        ST7735_INVON,  0,                   // 13: Don't invert display, no args
        ST7735_MADCTL,  1,                  // 14: Mem access ctl (directions), 1 arg:
            0xC0,                           //     row/col addr, bottom-top refresh
        ST7735_COLMOD,  1,                  // 15: set color mode, 1 arg, no delay:
            0x05                            //     16-bit color
    },
    Rcmd2green160x80[] = {              // 7735R init, part 2 (mini 160x80)
        2,                                  //  2 commands in list:
        ST7735_CASET,   4,                  //  1: Column addr set, 4 args, no delay:
            0x00, 0x1A,                     //     XSTART = 26
            0x00, 0x69,                     //     XEND = 105
        ST7735_RASET,   4,                  //  2: Row addr set, 4 args, no delay:
            0x00, 0x01,                     //     YSTART = 1
            0x00, 0xA0                      //     YEND = 160
    },
    // _colstart = 26;
    // _rowstart = 1;

    Rcmd3[] = {                         // 7735R init, part 3 (red or green tab)
        4,                                  //  4 commands in list:
        ST7735_GMCTRP1, 16      ,           //  1: Gamma Adjustments (pos. polarity), 16 args + delay:
            0x02, 0x1c, 0x07, 0x12,         //     (Not entirely necessary, but provides
            0x37, 0x32, 0x29, 0x2d,         //      accurate colors)
            0x29, 0x25, 0x2B, 0x39,
            0x00, 0x01, 0x03, 0x10,
        ST7735_GMCTRN1, 16      ,           //  2: Gamma Adjustments (neg. polarity), 16 args + delay:
            0x03, 0x1d, 0x07, 0x06,         //     (Not entirely necessary, but provides
            0x2E, 0x2C, 0x29, 0x2D,         //      accurate colors)
            0x2E, 0x2E, 0x37, 0x3F,
            0x00, 0x00, 0x02, 0x10,
        ST7735_NORON,   ST_CMD_DELAY,       //  3: Normal display on, no args, w/delay
            10,                             //     10 ms delay
        ST7735_DISPON,  ST_CMD_DELAY,       //  4: Main screen turn on, no args w/delay
            100                             //     100 ms delay
    };
ST7735::ST7735(uint32_t dcPort, uint16_t dcPin, uint32_t csPort, uint16_t csPin, uint32_t rstPort, uint16_t rstPin, uint32_t blPort, uint16_t blPin, int16_t w, int16_t h):
    EBT_GFX(w, h),
    _dcPort(dcPort),
    _csPort(csPort),
    _rstPort(rstPort),
    _blPort(blPort),
    _dcPin(dcPin),
    _csPin(csPin),
    _rstPin(rstPin),
    _blPin(blPin),
    _pwm(NULL){

    _width    = w;
    _height   = h;
    rotation  = 0;
    cursor_y  = cursor_x    = 0;
    padX = 0;

    addr_row = 0xFF;
    addr_col = 0xFF;

    gpio_clear(_rstPort, _rstPin);
    gpio_set(_csPort, _csPin);
    gpio_set(_dcPort, _dcPin);
    gpio_clear(_blPort, _blPin);
}

ST7735::ST7735(uint32_t dcPort, uint16_t dcPin, uint32_t csPort, uint16_t csPin, uint32_t rstPort, uint16_t rstPin, uint32_t blPort, uint16_t blPin, int16_t w, int16_t h, PWMClass* pwm):
    EBT_GFX(w, h),
    _dcPort(dcPort),
    _csPort(csPort),
    _rstPort(rstPort),
    _blPort(blPort),
    _dcPin(dcPin),
    _csPin(csPin),
    _rstPin(rstPin),
    _blPin(blPin),
    _pwm(pwm){

    _width    = w;
    _height   = h;
    rotation  = 0;
    cursor_y  = cursor_x    = 0;
    padX = 0;

    addr_row = 0xFF;
    addr_col = 0xFF;

    gpio_clear(_rstPort, _rstPin);
    gpio_set(_csPort, _csPin);
    gpio_set(_dcPort, _dcPin);
    gpio_clear(_blPort, _blPin);
}

void ST7735::displayInit(const uint8_t *addr){
    uint8_t numCommands, cmd, numArgs;
    uint16_t ms;

    numCommands = pgm_read_byte(addr++); // Number of commands to follow
    while (numCommands--) {              // For each command...
        cmd = pgm_read_byte(addr++);       // Read command
        numArgs = pgm_read_byte(addr++);   // Number of args to follow
        ms = numArgs & ST_CMD_DELAY;       // If hibit set, delay follows args
        numArgs &= ~ST_CMD_DELAY;          // Mask out delay bit
        sendCommand(cmd, addr, numArgs);
        addr += numArgs;

        if (ms) {
            ms = pgm_read_byte(addr++); // Read post-command delay time (ms)
            if (ms == 255)
            ms = 500; // If 255, delay for 500 ms
            delay(ms);
        }
    }
}

void ST7735::init(void){
    reset(); 
    delay(500);

    displayInit(Rcmd1);
    displayInit(Rcmd2green160x80);
    _colstart = 26;
    _rowstart = 1;
    displayInit(Rcmd3);

    uint8_t data = 0xC0;
    sendCommand(ST7735_MADCTL, &data, 1);

    setRotation(0);
    tabcolor = INITR_MINI160x80;

    if (_pwm != NULL){
        _pwm->init(250);
        _pwm->setDuty(0);
    }
}

void ST7735::startWrite(void){
    gpio_clear(_csPort, _csPin);
}

void ST7735::endWrite(void){
    gpio_set(_csPort, _csPin);
}

void ST7735::setRotation(uint8_t m){
    uint8_t madctl = 0;

    rotation = m & 3; // can't be higher than 3

    switch (rotation) {
    case 0:
        madctl = ST77XX_MADCTL_MX | ST77XX_MADCTL_MY | ST77XX_MADCTL_RGB;
        _height = 160;
        _width = 80;
        _xstart = _colstart;
        _ystart = _rowstart;
        break;
    case 1:
        madctl = ST77XX_MADCTL_MY | ST77XX_MADCTL_MV | ST77XX_MADCTL_RGB;
        _width = 160;
        _height = 80;
        _ystart = _colstart;
        _xstart = _rowstart;
        break;
    case 2:
        madctl = ST77XX_MADCTL_RGB;
        _height = 160;
        _width = 80;
        _xstart = _colstart;
        _ystart = _rowstart;
        break;
    case 3:
        madctl = ST77XX_MADCTL_MX | ST77XX_MADCTL_MV | ST77XX_MADCTL_RGB;
        _width = 160;
        _height = 80;
        _ystart = _colstart;
        _xstart = _rowstart;
    break;
    }

    sendCommand(ST7735_MADCTL, &madctl, 1);
}

void ST7735::reset(void){
    gpio_set(_rstPort, _rstPin);
    delay(5);
    gpio_clear(_rstPort, _rstPin);
    delay(20);
    gpio_set(_rstPort, _rstPin);
    delay(150);
}

void ST7735::backLightOn(){
    gpio_set(_blPort, _blPin);
}

void ST7735::backLightOff(){
    gpio_clear(_blPort, _blPin);
}

void ST7735::setDuty(uint8_t duty){
    if (_pwm != NULL){
        _pwm->setDuty(duty);
    }
}


void ST7735::sendCommand(uint8_t commandByte, const uint8_t *dataBytes, uint8_t numDataBytes) {
    gpio_clear(_csPort, _csPin);

    gpio_clear(_dcPort, _dcPin);        // Command mode
    ST7735_spi_out(commandByte);        // Send the command byte

    gpio_set(_dcPort, _dcPin);
    for (int i = 0; i < numDataBytes; i++) {
        ST7735_spi_out(pgm_read_byte(dataBytes++));
    }

    gpio_set(_csPort, _csPin);
}

void ST7735::writeCommand(uint8_t c){
    gpio_clear(_dcPort, _dcPin);
    ST7735_spi_out(c);
    gpio_set(_dcPort, _dcPin);
}

void ST7735::writeData(uint8_t c){
    gpio_set(_dcPort, _dcPin);
    ST7735_spi_out(c);
}

void ST7735::SPI_WRITE16(uint16_t w){
    ST7735_spi_out(w >> 8);
    ST7735_spi_out(w);
}

void ST7735::SPI_WRITE32(uint32_t l){
    ST7735_spi_out(l >> 24);
    ST7735_spi_out(l >> 16);
    ST7735_spi_out(l >> 8);
    ST7735_spi_out(l);
}

void ST7735::setAddrWindow(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
    x += _xstart;
    y += _ystart;
    uint32_t xa = ((uint32_t)x << 16) | (x + w - 1);
    uint32_t ya = ((uint32_t)y << 16) | (y + h - 1);

    writeCommand(ST7735_CASET); // Column addr set
    SPI_WRITE32(xa);

    writeCommand(ST7735_RASET); // Row addr set
    SPI_WRITE32(ya);

    writeCommand(ST7735_RAMWR); // write to RAM
}

void ST7735::drawPixel(int16_t x, int16_t y, uint16_t color){
    if ((x >= 0) && (x < _width) && (y >= 0) && (y < _height)) {
        frameBuffer[y * 80 + x] = color;
    }
}

void ST7735::writePixel(int16_t x, int16_t y, uint16_t color) {
    drawPixel(x, y, color);
}

void ST7735::show(void){
    // show(0,0,80,160);
    uint16_t *d = frameBuffer;

    startWrite();
    setAddrWindow(0,0,80,160);
    for (int i = 0; i < 12800; i++){
        SPI_WRITE16(*d++);
    }
    endWrite();
}

void ST7735::show(uint16_t x, uint16_t y, uint16_t w, uint16_t h){
    volatile uint16_t startAddress = y * 80 + x;
    volatile uint16_t numPixels = w * h;
    volatile uint16_t addressPointer = startAddress;

    uint16_t *d = frameBuffer + startAddress;

    startWrite();
    setAddrWindow(x,y,w,h);
    for (uint16_t i = h; i > 0; i--){
        for (uint16_t j = startAddress; j < startAddress + w; j++){
            SPI_WRITE16(frameBuffer[addressPointer++]);
        }
        addressPointer += (80 - w);
    }
    endWrite();
}

void ST7735::fillScreen(uint16_t color){
    for (int i = 0; i < 12800; i++){
        frameBuffer[i] = color;
    }
}
